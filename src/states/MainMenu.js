
BasicGame.MainMenu = function (game) {

	this.bg;
	this.spriteTopLeft;
	this.spriteTopRight;
	this.spriteBottomLeft;
	this.spriteBottomRight;

};

BasicGame.MainMenu.prototype = {

	create: function () {

	    this.bg = this.add.sprite(0, 0, 'atlas', 'MainMenuBG');

	    this.startButton = this.add.button(0, 0, 'atlas', this.onStartClicked, this,
	    	'PlayButton', 'PlayButton', 'PlayButton');
	    this.startButton.x = (this.game.width - this.startButton.width) / 2;
	    this.startButton.y = (this.game.height - this.startButton.height) * 2 / 3;

        this.settingsGroup = new SettingsGroup(this.game, 0xffffff, 
            this.game.height - 36, 36);
	},

	update: function () {

		//	Do some nice funky main menu effect here

	},

	onStartClicked: function() {
		this.state.start('Game');
	},

	resize: function (width, height) {

		//	If the game container is resized this function will be called automatically.
		//	You can use it to align sprites that should be fixed in place and other responsive display things.

	    this.bg.width = width;
	    this.bg.height = height;

	    this.spriteMiddle.x = this.game.world.centerX;
	    this.spriteMiddle.y = this.game.world.centerY;

	    this.spriteTopRight.x = this.game.width;
	    this.spriteBottomLeft.y = this.game.height;

	    this.spriteBottomRight.x = this.game.width;
	    this.spriteBottomRight.y = this.game.height;

	}

};
