MUTE_LARGE_FRAMES = {on: "sound-on48", off: "sound-off48"};
MUTE_SMALL_FRAMES = {on: "sound-on28", off: "sound-off28"};

MuteButton = function(game, atlas, isLarge) {
	this.frames = isLarge ? MUTE_LARGE_FRAMES : MUTE_SMALL_FRAMES;

	Phaser.Button.call(this, game, 0, 0, atlas, this.onClick, this,
		this.frames.on, this.frames.on, this.frames.on, this.frames.on);

	this.muted = false;
}

MuteButton.prototype = Object.create(Phaser.Button.prototype);
MuteButton.prototype.constructor = MuteButton;

MuteButton.prototype.onClick = function() {
	this.game.sound.mute = !this.game.sound.mute;
	var frame = this.game.sound.mute ? this.frames.off: this.frames.on;
	this.setFrames(frame, frame, frame, frame);
}