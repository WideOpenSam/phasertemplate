Meter = function(game, minValue, maxValue, startValue, 
	totalWidth, totalHeight, borderWidth, 
	borderColor, backgroundColor, fillColor) {

	this.game = game;
	this.minValue = minValue;
	this.maxValue = maxValue;
	this.value = startValue;
	this.totalWidth = totalWidth;
	this.totalHeight = totalHeight;
	this.borderWidth = borderWidth;
	this.borderColor = borderColor;
	this.backgroundColor = backgroundColor;
	this.fillColor = fillColor;
}

Meter.prototype = {
	create: function() {
		this.group = this.game.add.group();

		//create the 'unfilled in' background of the meter
		var backgroundBMD = this.game.add.bitmapData(this.totalWidth, this.totalHeight);
		console.log("background width, height: " + backgroundBMD.width + ", " + backgroundBMD.height);
	    backgroundBMD.ctx.beginPath();
	    backgroundBMD.ctx.rect(0, 0, this.totalWidth, this.totalHeight);
	    backgroundBMD.ctx.fillStyle = this.backgroundColor;
	    backgroundBMD.ctx.fill();
		var background = this.game.add.sprite(0, 0, backgroundBMD);
	    
	    //create the actual meter bitmap and sprite
		var progBmd = this.game.add.bitmapData(1, this.totalHeight);
	    progBmd.ctx.beginPath();
	    progBmd.ctx.rect(0, 0, 1, this.totalHeight);
	    progBmd.ctx.fillStyle = this.fillColor;
	    progBmd.ctx.fill();
	    // use the bitmap data as the texture for the sprite
	    this.progressBar = this.game.add.sprite(0, 0, progBmd);

	    //create the border bitmap and sprite. this is the largest
	    var graphics = this.game.add.graphics();
	    graphics.lineStyle(this.borderWidth, this.borderColor, 1.0);
	    graphics.drawRect(0, 0, this.totalWidth, this.totalHeight);
		/*
		var borderBMD = this.game.add.bitmapData(this.totalWidth, this.totalHeight);
	    borderBMD.ctx.beginPath();
	    borderBMD.ctx.rect(0, 0, this.totalWidth, this.totalHeight);
	    borderBMD.ctx.fillStyle = this.borderColor;
	    borderBMD.ctx.fill();
		var border = this.game.add.sprite(0, 0, borderBMD);
		*/

	    // this.group.add(border);
	    this.group.add(background);
	    this.group.add(this.progressBar);
	    this.group.add(graphics);
	},

	update: function() {

	}
}