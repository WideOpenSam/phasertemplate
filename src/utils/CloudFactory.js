CF_Direction = {
	UP: "going up", 
	DOWN: "top to bottom",
	LEFT: "right to left",
	RIGHT: "left to right",
}

CloudFactory = function(game, period, duration, numStart, frameOptions,
	bmpWidth = 0, bmpHeight = 0, bmpColor = "",
	direction = CF_Direction.LEFT) {
	this.game = game;
	this.period = period;
	this.duration = duration;
	this.numStart = numStart;
	this.frameOptions = frameOptions;
	this.bmpWidth = bmpWidth;
	this.bmpHeight = bmpHeight;
	this.bmpColor = bmpColor;

	this.direction = direction;
	this.isHorizontal = 
		(this.direction == CF_Direction.LEFT || 
			this.direction == CF_Direction.RIGHT);
}

CloudFactory.prototype = {
	create: function(group) {
		this.group = group;
	},

	createStartingClouds: function() {
		/* create clouds and start cloud timer */
		for(var i = 0; i < this.numStart; i++) {
			// var maxPos = this.isHorizontal ? this.game.width : this.game.height;
			this.createCloud(true);
		}
	},

	createCloud: function(startRandom = false) {
		var toPosition = new Phaser.Point();
		switch(this.direction) {
			case CF_Direction.UP:
				x = this.game.rnd.integerInRange(0, this.game.width);
				y = this.game.height;
				break;
			case CF_Direction.DOWN:
				x = this.game.rnd.integerInRange(0, this.game.width);
				y = -10;
				break;
			case CF_Direction.LEFT:
				x = this.game.width;
				y = this.game.rnd.integerInRange(0, this.game.height);
				break;
			case CF_Direction.RIGHT:
				x = -10;
				y = this.game.rnd.integerInRange(0, this.game.height);
				break;
		}

		if(startRandom) {
			x = this.game.rnd.integerInRange(0, this.game.width);
			y = this.game.rnd.integerInRange(0, this.game.height);
		}

		switch(this.direction) {
			case CF_Direction.UP:
				toPosition.x = x;
				toPosition.y = -10;
				break;
			case CF_Direction.DOWN:
				toPosition.x = x;
				toPosition.y = this.game.height;
				break;
			case CF_Direction.LEFT:
				toPosition.x = -10;
				toPosition.y = y;
				break;
			case CF_Direction.RIGHT:
				toPosition.x = this.game.width;
				toPosition.y = y;
				break;
		}

		var distance = Phaser.Point.distance(
			new Phaser.Point(x, y), toPosition);

		if(this.frameOptions.length > 0) {
			var frameIndex = Math.floor(Math.random() * this.frameOptions.length);	
			var frame = this.frameOptions[frameIndex];
			var cloud = this.game.add.sprite(x, y, 'atlas', frame);
		}
		else {	//create bitmap
			// console.log("creating bitmap");
			var bmd = this.game.add.bitmapData(this.bmpWidth, this.bmpHeight);
		    // draw to the canvas context like normal
		    bmd.ctx.beginPath();
		    bmd.ctx.rect(0, 0, this.bmpWidth, this.bmpHeight);
		    bmd.ctx.fillStyle = this.bmpColor;
		    bmd.ctx.fill();
		    // use the bitmap data as the texture for the sprite
		    var cloud = this.game.add.sprite(x, y, bmd);
		}
		// console.log("Frame: " + frame);
		this.group.addChild(cloud);

		var cloudTween = this.game.add.tween(cloud).to(
 	  		{x: toPosition.x, y: toPosition.y},
 	  		distance / Math.max(this.game.width, this.game.height) * this.duration, 
     		Phaser.Easing.Linear.None, true);
    	cloudTween.onComplete.add(function(){
     		cloud.destroy();}, this);
	},

	startTimer: function() {
		this.timer = this.game.time.create(false);
		this.timer.loop(this.period, this.createCloud, this);
		this.timer.start();
	},
	stopTimer: function() {
		this.timer.destroy();
	},

	onCloudTimer: function() {
		this.createCloud();
		this.startCloudTimer();
	},

	update: function() {

	},

	delete: function() {

	}
}