/**
 * A group to hold all the settings (fullscreen, mute button, etc)
 * Extends Phaser.Group
 * Author: Sam Potasznik
 */

SettingsGroup = function(game, bgColor, y, height = 48) {
	Phaser.Group.call(this, game);
	this.game = game;
	this.bgColor = bgColor;
	this.y = y;

	/*
	this.graphics = this.game.add.graphics(0, 0);
	this.add(this.graphics);
	this.graphics.lineStyle(2, 0x000000, 1);
	this.graphics.beginFill(bgColor);
	this.graphics.drawRect(0, 0, game.width, height);
	*/

	this.muteButton = new MuteButton(game, 'atlas', false);
	// this.muteButton.create();
	this.add(this.muteButton);
	this.muteButton.x = this.game.width - this.muteButton.width - 5;

	this.fsButton = new FullScreenButton(game, 'atlas', false);
	this.add(this.fsButton);
	this.fsButton.x = this.muteButton.x - this.fsButton.width - 10;
	this.fsButton.y = this.muteButton.y;
}

SettingsGroup.prototype = Object.create(Phaser.Group.prototype);
SettingsGroup.prototype.constructor = SettingsGroup;