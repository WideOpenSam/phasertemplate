function createRectangle(game, width, height, fillColor) {
	// create a new bitmap data object
    var bmd = game.add.bitmapData(width, height);

    // draw to the canvas context like normal
    bmd.ctx.beginPath();
    bmd.ctx.rect(0,0, width, height);
    bmd.ctx.fillStyle = fillColor;
    bmd.ctx.fill();

    // use the bitmap data as the texture for the sprite
    return sprite = game.make.sprite(0, 0, bmd);
}

function createCircle(game, radius, fillColor) {
	var bmd = game.add.bitmapData(2 * radius, 2 * radius);

    // draw to the canvas context like normal
    // bmd.ctx.beginPath();
    // bmd.ctx.circle(0, 0, radius);
    // bmd.ctx.fillStyle = fillColor;
    // bmd.ctx.fill();

    bmd.circle(radius, radius, radius, fillColor);

    // use the bitmap data as the texture for the sprite
    return sprite = game.make.sprite(0, 0, bmd);
}