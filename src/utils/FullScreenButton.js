FSB_LARGE_FRAMES = {fullscreen: "FullScreenIcon1", minimize: "FullScreenIcon2"};
FSB_SMALL_FRAMES = {fullscreen: "FullScreenIconSmall1", minimize: "FullScreenIconSmall2"};

FullScreenButton = function(game, atlas, isLarge) {
    this.frames = isLarge ? FSB_LARGE_FRAMES : FSB_SMALL_FRAMES;

	Phaser.Button.call(this, game, 0, 0, atlas, this.onFSClick, this, 
        this.frames.fullscreen, this.frames.fullscreen, this.frames.fullscreen, this.frames.fullscreen);
	
    this.game.scale.fullScreenScaleMode = Phaser.ScaleManager.SHOW_ALL;

    this.game.scale.onFullScreenChange.add(this.onFSChange, this);
}

FullScreenButton.prototype = Object.create(Phaser.Button.prototype);
FullScreenButton.prototype.constructor = FullScreenButton;

FullScreenButton.prototype.onFSClick = function() {
    if(this.game.scale.isFullScreen)
        this.game.scale.stopFullScreen();
    else {
        this.game.scale.startFullScreen();
    }
}

FullScreenButton.prototype.onFSChange = function() {
    var iconFrames = this.game.scale.isFullScreen ? this.frames.minimize : this.frames.fullscreen;
    this.setFrames(iconFrames, iconFrames, iconFrames);
}