/**
 * Takes in the game, sprite, and speed to start running
 * Returns a tween of the sprite running from one 
 * random side of the screen to another
 */
function startRunning(game, sprite, speed) {
	var startSide = game.rnd.integerInRange(0, 3);
	var destSide = game.rnd.integerInRange(0, 3);
	while(destSide == startSide) {
		destSide = game.rnd.integerInRange(0, 3);
	}

	var start = this.getRandomPointOnSide(startSide);
	var dest = this.getRandomPointOnSide(destSide);

	var distance = start.distance(dest);
	var duration = distance / RUN_SPEED;

	this.x = start.x;
	this.y = start.y;
	this.runTween = this.game.add.tween(this.spriteGroup).to(
		{x: dest.x, y: dest.y}, duration, 
		Phaser.Easing.Linear.None, true);
	this.runTween.onComplete.add(this.onRunComplete, this);
}

/**
 * takes in an int from 0-3 inclusive
 * returns random point along that wall (l, r, t, b)
 */
function getRandomPointOnSide(sideIndex, game) {
	var toReturn = new Phaser.Point();
	switch(sideIndex) {
		case 0: 	//left
			toReturn.x = -30; //-100;
			toReturn.y = game.rnd.integerInRange(100, 500);
			break;
		case 1: 	//right
			toReturn.x = 630;
			toReturn.y = game.rnd.integerInRange(100, 500);				
			break;
		case 2: 	//top
			toReturn.x = game.rnd.integerInRange(100, 450);
			toReturn.y = -30; //-115;
			break;
		default: 	//bottom
			toReturn.x = game.rnd.integerInRange(100, 450);
			toReturn.y = 630;
			break;
	}
	return toReturn;
}

// http://phaser.io/examples/v2/p2-physics/accelerate-to-object#gv
//accelerate obj1 to obj2 @ speed
function accelerateToObject(game, obj1, obj2, speed) {
    if (typeof speed === 'undefined') { 
    	speed = 60; 
    }
    var angle = Math.atan2(obj2.y - obj1.y, obj2.x - obj1.x);
    obj1.body.rotation = angle + game.math.degToRad(90);  // correct angle of angry bullets (depends on the sprite used)
    obj1.body.force.x = Math.cos(angle) * speed;    // accelerateToObject 
    obj1.body.force.y = Math.sin(angle) * speed;
}

function pointToString(point) {
	return "(" + point.x + ", " + point.y + ")";
}

function centerText(text) {
	text.x = Math.floor((text.parent.width - text.width) / 2);
}