# README #


### What is this repository for? ###

* A blank template for quick-starting Phaser games based on Photonstorm's templates. This comes with 4 states: Boot, Preloader, MainMenu, and Game as well as background assets and a packed texture.
* Version 1.0

### How do I get set up? ###

* Download via clone or zip
* Start a python server in the base directory (via command line on Windows: python -m http.server 8000)
* Navigate in a browser to http://localhost:8000/

### Contribution guidelines ###

* ???

### Who do I talk to? ###

* @SamPotasz on twitter!